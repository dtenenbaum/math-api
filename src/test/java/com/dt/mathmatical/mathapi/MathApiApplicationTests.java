package com.dt.mathmatical.mathapi;

import com.dt.mathmatical.mathapi.model.ExpressionInput;
import com.dt.mathmatical.mathapi.service.ExpressionService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.inject.Inject;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@SpringBootTest
class MathApiApplicationTests {

    @Inject
    ExpressionService expressionService;

    List<String> validPostfixInput = Arrays.asList(
        "1 2 +",
        "3 1 2 * +",
        "5 6 * 7 8 / + 9 -"
    );

    Map<String, String> validPrefixInput = Stream.of(
        new AbstractMap.SimpleEntry<>("+ 1 2", "1 2 +"),
        new AbstractMap.SimpleEntry<>("+ 3 * 1 2 ", "3 1 2 * +"),
        new AbstractMap.SimpleEntry<>("- + * 5 6 / 7 8 9", "5 6 * 7 8 / + 9 -")
    ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));

    Map<String, String> validInfixInput = Stream.of(
        new AbstractMap.SimpleEntry<>("1 + 2", "1 2 +"),
        new AbstractMap.SimpleEntry<>("3 + 1 * 2 ", "3 1 2 * +"),
        new AbstractMap.SimpleEntry<>("5 * 6 + 7 / 8 - 9", "5 6 * 7 8 / + 9 -")
    ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));

    @Test
    void postFixValidationSuccess() {
        validPostfixInput.forEach(input -> {
            try {
                assertEquals(input, expressionService.toPostfix(new ExpressionInput(input)).getResultExpression(), "output does not match input");
            } catch (Exception e) {
                e.printStackTrace();
                fail("Exception when input is: " + input, e);
            }
        });
    }

    @Test
    void prefixValidationSuccess() {
        validPrefixInput.forEach((key, value) -> {
            try {
                assertEquals(value, expressionService.toPostfix(new ExpressionInput(key)).getResultExpression(), "output does not match expected output");
            } catch (Exception e) {
                e.printStackTrace();
                fail("Exception when input is: " + key, e);
            }
        });
    }

    @Test
    void infixValidationSuccess() {
        validInfixInput.forEach((key, value) -> {
            try {
                assertEquals(value, expressionService.toPostfix(new ExpressionInput(key)).getResultExpression(), "output does not match expected output");
            } catch (Exception e) {
                e.printStackTrace();
                fail("Exception when input is: " + key, e);
            }
        });
    }

}
