package com.dt.mathmatical.mathapi.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "The expression input definition; Just the plaintext expression for now")
public class ExpressionInput {

    @ApiModelProperty(value = "An expression in infix, prefix, or postfix notation", example = "1 + 2 * 3")
    private String expression;

}
