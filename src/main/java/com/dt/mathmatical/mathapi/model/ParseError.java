package com.dt.mathmatical.mathapi.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

@Value
@ApiModel(description = "The error returned when the input expression cannot be parsed")
public class ParseError {

    @ApiModelProperty(value = "The HTTP Status Code", example = "400")
    private final int status;

    @ApiModelProperty(value = "The HTTP Status Code", example = "Bad Request")
    private final String message;

    @ApiModelProperty(value = "The input expression that had parse errors", example = "+ + 1")
    private final String originalExpression;

    @ApiModelProperty(value = "Details abot what went wrong", example = "Not enough operands for operator: +")
    private final String details;

    @ApiModelProperty(value = "The inferred input notation type", example = "PREFIX")
    private final String notationType;

}
