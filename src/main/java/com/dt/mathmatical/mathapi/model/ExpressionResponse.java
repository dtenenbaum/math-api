package com.dt.mathmatical.mathapi.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "A result representing the expression in postfix notation")
public class ExpressionResponse {

    @ApiModelProperty(value = "The original expression in postfix notation", example = "1 2 3 * +")
    private String resultExpression;

    @ApiModelProperty(value = "The original expression", example = "1 + 2 * 3")
    private String originalExpression;

    @ApiModelProperty(value = "The original expression's notation type.", example = "PREFIX")
    private String originalNotationType;

}
