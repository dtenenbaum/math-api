package com.dt.mathmatical.mathapi.service;

import com.dt.mathmatical.mathapi.exception.ExpressionParseException;
import com.dt.mathmatical.mathapi.model.ExpressionInput;
import com.dt.mathmatical.mathapi.model.ExpressionResponse;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The service which converts expressions to postfix notation.
 */
@Service
public class ExpressionService {

    /**
     * THis map contains all supported operators, as well as their precedence
     */
    public static final Map<String, Integer> OPERATORS = Stream.of(
        new AbstractMap.SimpleEntry<>("+", 1),
        new AbstractMap.SimpleEntry<>("-", 1),
        new AbstractMap.SimpleEntry<>("*", 2),
        new AbstractMap.SimpleEntry<>("/", 2)
    ).collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));

    enum ExpressionParseState {
        EXPECT_OPERATOR,
        EXPECT_OPERAND
    }

    public enum ExpressionNotationType {
        PREFIX,
        POSTFIX,
        INFIX,
        UNKNOWN
    }

    private List<String> tokenize(String expression) {
        return Arrays.stream(expression.split(" "))
            .filter(Predicate.not(String::isBlank))
            .collect(Collectors.toList());
    }

    private String tokensToString(List<String> tokens) {
        return String.join(" ", tokens);
    }

    /**
     * Infers the notation of an inout expression, Then attempts to parse it and convert it to postfix notation
     *
     * @param expression the expression to convert to postfix
     * @return the result of the conversion, including the postfix expression
     * @throws ExpressionParseException if the exception could not be converted
     */
    public ExpressionResponse toPostfix(ExpressionInput expression) throws ExpressionParseException {
        List<String> tokens = tokenize(expression.getExpression());
        ExpressionNotationType type = inferNotationType(tokens);
        switch (type) {
            case PREFIX:
                return new ExpressionResponse(
                    tokensToString(prefixToPostfix(tokens)),
                    tokensToString(tokens),
                    type.name());
            case POSTFIX:
                if (isValidPostfix(tokens)) {
                    return new ExpressionResponse(
                        tokensToString(tokens),
                        tokensToString(tokens),
                        type.name());
                } else {
                    throw new ExpressionParseException("Expression seemed to be postfix, but was of an invalid format.", ExpressionNotationType.POSTFIX,
                        tokensToString(tokens));
                }
            case INFIX:
                return new ExpressionResponse(
                    tokensToString(infixToPostfix(tokens)),
                    tokensToString(tokens),
                    type.name());
            default:
                throw new ExpressionParseException("Expression is in an unknown form.", ExpressionNotationType.UNKNOWN, tokensToString(tokens));
        }
    }

    private ExpressionNotationType inferNotationType(List<String> tokens) throws ExpressionParseException {
        if (tokens == null || tokens.size() < 3) {
            throw new ExpressionParseException("An expression must contain at least 3 tokens.", ExpressionNotationType.UNKNOWN, tokensToString(tokens));
        }

        String firstToken = tokens.get(0);
        String lastToken = tokens.get(tokens.size() - 1);
        if (isOperand(firstToken) && isOperand(lastToken)) {
            return ExpressionNotationType.INFIX;
        } else if (isOperand(firstToken) && isOperator(lastToken)) {
            return ExpressionNotationType.POSTFIX;
        } else if (isOperator(firstToken) && isOperand(lastToken)) {
            return ExpressionNotationType.PREFIX;
        } else {
            return ExpressionNotationType.UNKNOWN;
        }
    }

    private boolean isOperator(String possibleOperator) {
        return OPERATORS.containsKey(possibleOperator);
    }

    private boolean isOperand(String possibleOperand) {
        try {
            int a = Integer.parseInt(possibleOperand);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    private boolean isValidPostfix(List<String> postfixTokens) throws ExpressionParseException {
        int stackCounter = 0;
        for (String token : postfixTokens) {
            if (isOperand(token)) {
                stackCounter++;
            } else if (isOperator(token)) {
                stackCounter -= 2;
                if (stackCounter < 0) {
                    return false;
                }
                stackCounter++;
            } else {
                throw new ExpressionParseException("Invalid token: " + token, ExpressionNotationType.POSTFIX,
                    tokensToString(postfixTokens));
            }
        }
        return stackCounter == 1;
    }

    private List<String> prefixToPostfix(List<String> prefixTokens) throws ExpressionParseException {
        Stack<List<String>> operandStack = new Stack<>();
        ArrayList<String> prefixTokensReversed = new ArrayList<>(prefixTokens);
        Collections.reverse(prefixTokensReversed);
        for (String token : prefixTokensReversed) {
            if (isOperand(token)) {
                operandStack.push(Collections.singletonList(token));
            } else if (isOperator(token)) {
                if (operandStack.size() < 2) {
                    throw new ExpressionParseException("Not enough operands for operator: " + token,
                        ExpressionNotationType.PREFIX, tokensToString(prefixTokens));
                }
                operandStack.push(Stream.of(operandStack.pop(), operandStack.pop(), Collections.singletonList(token))
                    .flatMap(Collection::stream).collect(Collectors.toList()));
            } else {
                throw new ExpressionParseException("Invalid token: " + token, ExpressionNotationType.PREFIX,
                    tokensToString(prefixTokens));
            }
        }

        if (operandStack.size() > 1) {
            throw new ExpressionParseException("Dangling operator, not enough operands",
                ExpressionNotationType.PREFIX, tokensToString(prefixTokens));
        }

        return operandStack.get(0);
    }

    /**
     * Implementation of Shunting Yard algorithm
     */
    private List<String> infixToPostfix(List<String> infixTokens) throws ExpressionParseException {
        Stack<String> operatorStack = new Stack<>();
        List<String> output = new ArrayList<>();
        ExpressionParseState parseState = ExpressionParseState.EXPECT_OPERAND;

        for (String token : infixTokens) {
            if (isOperand(token)) {
                if (parseState != ExpressionParseState.EXPECT_OPERAND) {
                    throw new ExpressionParseException("Expected an operand, but got operator: " + token, ExpressionNotationType.INFIX,
                        tokensToString(infixTokens));
                }
                output.add(token);
                parseState = ExpressionParseState.EXPECT_OPERATOR;
            } else if (isOperator(token)) {
                if (parseState != ExpressionParseState.EXPECT_OPERATOR) {
                    throw new ExpressionParseException("Expected an operator, but got operand: " + token, ExpressionNotationType.INFIX,
                        tokensToString(infixTokens));
                }
                while (!operatorStack.isEmpty() && OPERATORS.get(token) <= OPERATORS.get(operatorStack.peek())) {
                    output.add(operatorStack.pop());
                }
                operatorStack.push(token);
                parseState = ExpressionParseState.EXPECT_OPERAND;
            } else {
                throw new ExpressionParseException("Invalid token: " + token, ExpressionNotationType.INFIX,
                    tokensToString(infixTokens));
            }
        }

        //Add remaining operators from the stack
        Collections.reverse(operatorStack);
        output.addAll(operatorStack);

        return output;
    }

}
