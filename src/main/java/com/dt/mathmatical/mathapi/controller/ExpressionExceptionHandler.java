package com.dt.mathmatical.mathapi.controller;

import com.dt.mathmatical.mathapi.exception.ExpressionParseException;
import com.dt.mathmatical.mathapi.model.ParseError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExpressionExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = ExpressionParseException.class)
    public ResponseEntity<ParseError> handleException(ExpressionParseException exception) {
        return ResponseEntity
            .status(HttpStatus.BAD_REQUEST)
            .body(new ParseError(
                HttpStatus.BAD_REQUEST.value(),
                HttpStatus.BAD_REQUEST.getReasonPhrase(),
                exception.getOriginalExpression(),
                exception.getMessage(),
                exception.getNotationType().name()));
    }
}
