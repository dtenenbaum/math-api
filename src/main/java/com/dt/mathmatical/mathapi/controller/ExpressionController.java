package com.dt.mathmatical.mathapi.controller;

import com.dt.mathmatical.mathapi.model.ExpressionResponse;
import com.dt.mathmatical.mathapi.service.ExpressionService;
import io.swagger.annotations.*;
import com.dt.mathmatical.mathapi.model.ExpressionInput;
import com.dt.mathmatical.mathapi.model.ParseError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
public class ExpressionController {

    Logger logger = LoggerFactory.getLogger(ExpressionController.class);

    final ExpressionService expressionService;

    @Inject
    public ExpressionController(ExpressionService expressionService) {
        this.expressionService = expressionService;
    }

    @ApiOperation(value = "Convert an expression to postfix notation", response = ExpressionInput.class)
    @ApiResponses({
        @ApiResponse(code = 200, message = "Expression successfully parsed and converted to postfix."),
        @ApiResponse(code = 400, message = "Bad Request: Parsing Error.",
            response = ParseError.class)
    })
    @PostMapping(value = "/to-postfix")
    public ExpressionResponse toPostfix(@RequestBody ExpressionInput expressionInputDefinition) throws Exception {
        logger.info("Received a Request to format this input: [" + expressionInputDefinition.getExpression() + "]");
        return expressionService.toPostfix(expressionInputDefinition);
    }

}
