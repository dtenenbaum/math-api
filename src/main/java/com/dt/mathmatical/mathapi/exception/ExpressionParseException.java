package com.dt.mathmatical.mathapi.exception;

import com.dt.mathmatical.mathapi.service.ExpressionService;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.convert.DataSizeUnit;

public class ExpressionParseException extends RuntimeException{

    @Getter
    @Setter
    ExpressionService.ExpressionNotationType notationType;

    @Getter
    @Setter
    String originalExpression;

    public ExpressionParseException(String message, ExpressionService.ExpressionNotationType notationType,
                                    String originalExpression) {
        super(message);
        this.notationType = notationType;
        this.originalExpression = originalExpression;
    }
}
