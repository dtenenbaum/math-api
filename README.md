
# Math Expression API
A REST Service by *Drew Tenenbaum* using built using [Spring Boot](https://spring.io/projects/spring-boot) and [Gradle](https://gradle.org/). 
Created for the JVM + .NET code challenge 2020, This API allows POST requests that convert expressions from `infix`,`prefix`,`postfix` notation to `postfix` notation.

For example, `3 + 2 * 7` will be converted to `3 2 7 * +`

### Requirements
You need to have Java 11 installed, as well as your JAVA_HOME and PATH setup correctly.

## Starting the Server
Navigate to the root directory of the project, and invoke the `bootRun` task of the Gradle wrapper:
```
$ gradlew bootRun
```
You can also run the test suite:
```
$ gradlew test
```
After startup, server is now running on `localhost:8080`

You can view the application logs in `logs/log.log`

## REST Endpoints

Once the server is running, you can reach the swagger documentation, which details the Request/Response contract.
 [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

### Endpoints

 - **/to-postfix**
 -- *Method Type*: POST
 -- *Description* : Converts a space delimited expression to postfix notation


### Examples

Valid Input:
```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"expression":"1 + 2 * 6"}' \
  http://localhost:8080/to-postfix
```
```
{
    "resultExpression": "1 2 6 * +",
    "originalExpression": "1 + 2 * 6",
    "originalNotationType": "INFIX"
}
```

Invalid Input:
```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"expression":"1 + 2 *"}' \
  http://localhost:8080/to-postfix
```
```
{
    "status": 400,
    "message": "Bad Request",
    "originalInput": "1 + 2 *",
    "error": "Parsing Error",
    "details": "Expression seemed to be postfix, but was of an invalid format.",
    "notationType": "POSTFIX"
}
```